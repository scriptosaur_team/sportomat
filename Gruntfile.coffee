module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")

    connect:
      server:
        port: 9001
        base: 'html'


    less:
      development:
        options:
          compress: false
          sourceMap: true
          sourceMapURL: "/css/main.css.map"
        files:
          "html/css/main.css": "html/less/main.less"
      production:
        options:
          compress: true
          sourceMap: false
        files:
          "html/css/main.css": "html/less/main.less"


    watch:
      less:
        files: "html/less/**/*.less"
        tasks: ["less:development"]


  grunt.loadNpmTasks "grunt-connect"
  grunt.loadNpmTasks "grunt-contrib-less"
  grunt.loadNpmTasks "grunt-contrib-watch"


  grunt.registerTask "default", ["less:development", "watch"]
  grunt.registerTask "build", ["less:production"]
  grunt.registerTask "server", ["connect"]
