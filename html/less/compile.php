<?php
require('less.php/Less.php');

$less = new Less_Parser(['compress'=>true]);
$less->parseFile('main.less');

file_put_contents( '../css/main.css', $less->getCss() );
