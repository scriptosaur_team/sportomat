jQuery(function($){
	// On Page Resize {
	function on_page_resize(is_onload) {
		var width = parseInt( $(window).width() );

		// Fix map width and height {
		if ( $('.catalog-page .cp-col-right iframe').length ) {
			$('.catalog-page .cp-col-right').width( width - 300 );
			$('.catalog-page .cp-col-right iframe').height( $(window).height() - 40 );
		}
		// }

		// Fix popup margins {
		if ( is_onload )
			$('.popup').width( $('.popup').width() ).css('display', 'block');
		else
			$('.popup').width( $('.popup').width() );
		// }

		// >640
		if ( width > 640 && ( $('body').data('width') !== 940 ) ) {
			$('.mptc-news-cols .mptc-news-col:last-child')
				.append( $('.mptc-sport-news') )
				.append( $('.mptc-sport-posts') );

			$('.optc-right-col').insertAfter('.optc-left-col');

			$('body').data('width', 940);
		}
		// <=640
		if ( width > 320 && width <= 640 && ( $('body').data('width') !== 640 ) ) {
			$('.mptc-sport-news').insertAfter('.mptc-objects');
			$('.mptc-sport-posts').insertAfter('.mptc-sport-videos');

			$('.optc-right-col').insertAfter('.optclc-tabs-content');

			$('body').data('width', 640);
		}
		// <=320
		if ( width <= 320 && ( $('body').data('width') !== 320 ) ) {
			$('body').data('width', 320);
		}
	}
	on_page_resize(true);

	$(window).resize(function(){
		on_page_resize(false);
	});
	// }

	//$(window).scroll(function(){
		//var offset = $(window).scrollTop();
		//offset = parseInt(offset);

		//if ( offset >= 250 ) {
			//offset = 250 - offset;
			//$('.mpfs-bg').css('backgroundPosition', 'center top '+offset+'px');
		//}
	//});

	// Select emulation {
	$('.select-emul').click(function(event){
		event.stopPropagation();

		$(this).toggleClass('se-active');
		$(this).find('.se-list').toggle();

		if ( $(this).hasClass('se-active') ) {
			if ( $(this).hasClass('se-mini') )
				$(this).find('.se-list').width( $(this).find('input[readonly]').width() + 16 );
			else
				$(this).find('.se-list').width( $(this).find('input[readonly]').width() + 18 );
		}
	});

	$('.select-emul li').click(function(){
		var $visibleInput = $(this).parent().parent().find('input[readonly]');
		$visibleInput.val( $(this).text() );
		$visibleInput.trigger('change');

		$(this).parent().parent().find('input[type="hidden"]').val( $(this).data('val') );
	});

	$('html').click(function(){
		var $activeSelect = $('.select-emul.se-active');
		$activeSelect.removeClass('se-active');
		$activeSelect.find('.se-list').hide();
	});
	// }

	// Checkbox emulation {
	$('.checkbox-emul').click(function(){
		if ( $(this).hasClass('checked') ) {
			$(this).removeClass('checked');
			$(this).find('input').val('');
		} else {
			$(this).addClass('checked');
			$(this).find('input').val( $(this).data('val') );
		}
	});
	// }

	// Radio emulation {
	$('.radio-emul').click(function(){
		$input = $(this).find('input');
		$inputGroup = $('input[name='+$input.attr('name')+']');
		$inputGroup.each(function(){
			$(this)
				.prop('checked', false)
				.closest('.radio-emul')
				.removeClass('checked');
		});
		$(this)
			.addClass('checked')
			.find('input')
			.prop('checked', true);
	});
	// }

	// Tabs {
	$('.js-tab').click(function(event){
		event.preventDefault();

		$(this).parent().find('.js-tab').each(function(){
			$( $(this).data('target') ).hide();
			$(this).removeClass('js-tab-active');
		});

		$( $(this).data('target') ).show();
		$(this).addClass('js-tab-active');
	});
	// }

	// Read more links {
	$('.js-read-more').click(function(event){
		event.preventDefault();

		$(this).toggleClass('jsrm-active');
		$( $(this).data('target') ).toggle();

		var $text = $(this).find('span');
		var hint = $text.text();
		$text.text( $text.data('alttext') );
		$text.data('alttext', hint);
	});
	// }

	// Header menu {
	$('.header-menu a').click(function(event){
		var width = $(window).width();

		if ( $(this).attr('href') === '#' )
			event.preventDefault();

		if (width <= 400 && $(this).hasClass("hm-user-login")) {
			if ($(this).hasClass('hm-mi-active')) {
				$(this).removeClass('hm-mi-active');
				$($(this).data('target')).removeClass('hum-active');
			} else {
				$(this).addClass('hm-mi-active');
				$($(this).data('target')).addClass('hum-active');
			}
		} else {
			if ($(this).hasClass('hm-mi-active')) {
				$(this).removeClass('hm-mi-active');
				$($(this).data('target')).removeClass('hum-active');
				$(".page-header").removeClass("h-top-menu-open");
			} else {
				$('.hm-mi-active').removeClass('hm-mi-active');
				$('.hum-active').removeClass('hum-active');

				$(this).addClass('hm-mi-active');
				$($(this).data('target')).addClass('hum-active');
				$(".page-header").addClass("h-top-menu-open");
			}
		}

		// Fixes
		if ( $(this).hasClass('hm-user-login') )
			$('.h-user-menu input[type="email"]').focus();

		if ( $(this).hasClass('hm-user-logged') ) {
			var width = parseInt( $(this).width() );

			if ( width > 98 )
				$('.h-user-menu.hum-logged').width( width + 32 );
		}
		if ( $('.h-top-menu.hum-active').length ) {
			$('.margin-fixed').removeClass('margin-fixed').addClass('margin-fix');
		} else {
			$('.margin-fix').removeClass('margin-fix').addClass('margin-fixed');
		}
	});
	// }

	//$('.optcat-tabs a').click(function(event){
	$('.optc-activities-types').on('click', '.optcat-tabs a', function(event){
		event.preventDefault();

		if ( $(this).hasClass('optcatt-active') ) {
			$(this).removeClass('optcatt-active');
			$(this).parent().parent().find( $(this).data('target') ).hide();
		} else {
			$(this).parent().parent().find('.optcatt-active').removeClass('optcatt-active');

			$(this).parent().find('a').each(function(){
				$(this).parent().parent().find( $(this).data('target') ).hide();
			});

			$(this).addClass('optcatt-active');
			$(this).parent().parent().find( $(this).data('target') ).show();
		}
	});

	// Fix image hint {
	$('.ppctr-img-with-hint').each(function(){
		var $hint = $(this).find('span');

		$hint.width( $(this).find('img').width() - 40 );

		var mt = $hint.height() + 30;
		mt = '-'+mt+'px';

		$hint.css('marginTop', mt);
	});
	// }

	$('.popup-open').click(function(event){
		event.preventDefault();
		$('.popup-overlay').show();
		$('.aptc-object-data').height( $(window).height() / 100 * 80 );
	});
	$('.popup-close').click(function(event){
		event.preventDefault();
		$('.popup-overlay').hide();
	});

	$('.popup-overlay').show();
	$('.aptc-object-data, .aptc-photo, .aptc-video').height( $(window).height() / 100 * 80 );
});
